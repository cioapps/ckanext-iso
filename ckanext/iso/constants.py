
TYPE_ISO = 'iso'
TYPE_ISO19139 ='iso19139'

SUPPORTED_ISO_INPUT_TYPES = [ TYPE_ISO, TYPE_ISO19139 ] 

SUPPORTED_DATASET_FORMATS = [ TYPE_ISO ]

TYPE_ISO_RESOURCE_ONLINE_RESOURCE = 'online-resource'
TYPE_ISO_RESOURCE_GRAPHIC_OVERVIEW = 'graphic-overview'

TYPE_ISO_RESOURCE_METADATA_CONTACT = 'metadata-contact'
TYPE_ISO_RESOURCE_DISTRIBUTOR = 'distributor'
TYPE_ISO_RESOURCE_MAINTAINER = 'resource-maintainer'
TYPE_ISO_RESOURCE_RESOURCE_CONTACT = 'resource-contact'
TYPE_ISO_RESOURCE_CITED_RESPONSIBLE_PARTY = 'cited-responsible-party'

SUPPORTED_ISO_RESOURCE_FORMATS = [
    TYPE_ISO_RESOURCE_ONLINE_RESOURCE,
    TYPE_ISO_RESOURCE_GRAPHIC_OVERVIEW,
    TYPE_ISO_RESOURCE_DISTRIBUTOR,

    TYPE_ISO_RESOURCE_METADATA_CONTACT,
    TYPE_ISO_RESOURCE_MAINTAINER,
    TYPE_ISO_RESOURCE_RESOURCE_CONTACT,
    TYPE_ISO_RESOURCE_CITED_RESPONSIBLE_PARTY
    ]